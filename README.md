# True Incremental Reading - Anki
## Attention!
This add-on is far from completion. If you read this text, it means it is not even usable currently. I decided to make the Git open anyway, so other people can take a look at progress etc.

Consider this as a pre-pre-alpha-wip. Do NOT download this.

Licenses etc are not yet tidyed up, but will (hopefully) on first alpha "release".

## Introduction
Inspired by SuperMemo, this Add-on brings proper support for incremental reading to Anki.

It will allow you to add cards, which, upon being reviewed, open the Add Cards dialog in a splitscreen. This splitscreen can then be used to watch YouTube videos, extract texts, view PDFs.

## Sneak preview :)
![image](https://user-images.githubusercontent.com/7038116/85288045-683a4f00-b495-11ea-9a8e-ee47564ccd59.png)

## Licenses and credits
© 2020 p4nix

Anki True Incremental Reading is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details. For more information please see the LICENSE file that accompanied this program.

This code makes use of:
* jquery
* pdfjs
* tinymce
* lovac42's Anki libs
* parts from fonol's anki-search-inside-add-card

## Thanks to
fonol, lovac42, DavidA, Arthur Milchior, glutanimate, Piotr Wozniak and all the others of whom I got inspiration or copy-pasted from.

This list is not complete.
