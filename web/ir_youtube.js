// Y O U T U B E C O N T E N T

var YTplayer;

var startPlayAt = 0;
var stopPlayAt = -1;
var stopPlayTimer;

function loadYouTube(){
  document.getElementById("htmlcontent").innerHTML = `
  <style>
    #video_wrapper {
      position: relative;
      padding-bottom: 56.25%;
    }
    #youtubeiframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
    #youtube-timing-table {
      width: auto;
      margin: 0 auto;

    }
    html, body,input {
      font-size: 1.05em;

    }
  </style>
  <div id="video_wrapper">
    <div id="youtubeiframe"></div>
  </div>
  <br>
  <table id="youtube-timing-table">
  <thead>
    <tr>
      <th></th>
      <th>Start</th>
      <th>Stop</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Video:</td>
      <td>
        <input id="input_startplay" type="time" min="00:00:00" step="1" value="00:00:00">
        <button onclick="setTimeCurr('input_startplay')">&darr;</button>
      </td>
      <td>
        <input id="input_pauseplay" type="time" min="00:00:00" step="1" value="00:00:00">
        <button onclick="setTimeCurr('input_pauseplay')">&darr;</button>
      </td>
      <td>button</td>
    </tr>
    <tr>
      <td>Extract:</td>
      <td>form
      but</td>
      <td>form
      button</td>
      <td>button</td>
    </tr>
  </tbody>
  </table>
  `;

  parseYouTubeData();

  var tag;
  var firstScriptTag;
  tag = document.createElement('script');

  tag.src = "https://www.youtube.com/iframe_api";
  firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


}

function setTimeCurr(id){
  var time = TimeFromInt(YTplayer.getCurrentTime());

  document.getElementById(id).value = time;

}

function TimeFromInt(num){
  var sec_num = parseInt(num, 10); // don't forget the second param
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  return hours+':'+minutes+':'+seconds;
}

function IntFromTime(time){
  var hms = time;   // your input string
  var a = hms.split(':'); // split it at the colons

  // minutes are worth 60 seconds. Hours are worth 60 minutes.
  var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
  return seconds;
}

function parseYouTubeData(maximum_value){
  let regex_getStartAt = /start:(\d+)/g;
  let regex_getPauseAt = /stop:(\d+)/g;

  startplay_string = global_data.match(regex_getStartAt)[0];
  stopplay_string = global_data.match(regex_getPauseAt)[0];


  if(startplay_string){
    startPlayAt = parseInt(startplay_string.replace(regex_getStartAt, "$1"));
  }

  if(stopplay_string){
    stopPlayAt = parseInt(stopplay_string.replace(regex_getPauseAt, "$1"));
  }

  document.getElementById("input_startplay").value = TimeFromInt(startPlayAt);
  document.getElementById("input_pauseplay").value = TimeFromInt(stopPlayAt);

}

function onYouTubeIframeAPIReady() {
  YTplayer = new YT.Player('youtubeiframe', {
    videoId: global_content,
    suggestedQuality: 'highres',
    events: {
      'onStateChange': onStateChange,
      'onReady': function (ev) {
        ev.target.seekTo(startPlayAt);
        parseYouTubeData();
      }
    }
  });
}

function onStateChange(event){
  var time, rate, remainingTime;
  clearTimeout(stopPlayTimer);

  if (event.data == YT.PlayerState.PLAYING) {
    time = YTplayer.getCurrentTime();
    // Add .4 of a second to the time in case it's close to the current time
    // (The API kept returning ~9.7 when hitting play after stopping at 10s)
    if (time + .4 < stopPlayAt) {
      rate = YTplayer.getPlaybackRate();
      remainingTime = (stopPlayAt - time) / rate;
      stopPlayTimer = setTimeout(pauseVideo, remainingTime * 1000);
    }
  }
}

function pauseVideo() {
    YTplayer.pauseVideo();
  }


function saveYouTube(){
  // Get values from the two important fields
  startPlayAt = IntFromTime(document.getElementById("input_startplay").value);
  stopPlayAt  = IntFromTime(document.getElementById("input_pauseplay").value);

  global_data = "start:"+startPlayAt+"stop:"+stopPlayAt;
}
