// H T M L C O N T E N T
function loadHTML(){
  document.getElementById("htmlcontent").innerHTML = `
  <style>


  </style>
  <textarea id="tir-tinymce"></textarea>
  `;


  tinymce.init({
    selector: '#tir-tinymce',
    plugins: 'anchor, link',
    menubar: false,
    resize : false,
    height: '100%',
    width: '100%',
    toolbar:
      "undo redo | styleselect | bold italic removeformat |"+
      " forecolor backcolor | extractbutton clozebutton",

    content_style:
      ".ir-extract {background-color:#" +global_extractcolor + ";}"+
      ".ir-cloze   {background      :#" +global_clozecolor   + ";}",

    setup: (editor) => {
      editor.on('keyup', runMark);

      //Add shortcut for extracts
      editor.ui.registry.addButton('extractbutton', {
        text: "Extract",
        tooltip: "Extract selection. "+global_sc_extract,
        onAction: function(e) {
          tinyMCEdoExtract(editor);
      }});

      // Add shortcut for
      editor.ui.registry.addButton('clozebutton', {
        text: "Cloze",
        tooltip: "Cloze deletion. "+global_sc_cloze,
        onAction: function(e) {
          tinyMCEdoCloze(editor);
      }});


      editor.on('init', function (e) {
        editor.setContent(global_content);
      });
    },

    init_instance_callback: "runMark"
  });
}

function runMark() {
  var editor = tinyMCE.activeEditor;
  var rawText = editor.getContent({
    format: 'text'
  });

  var sentenceArray = rawText.match(/{{c(\d+)::([\s\S]*?)}}/g);
  var matchWarning = [];
  var output;

  for (var i in sentenceArray) {
     matchWarning.push(sentenceArray[i]);
  }


  // Store the selection
  var bookmark = editor.selection.getBookmark();

  // Remove previous marks and add new ones
  $(editor.getBody()).unmark().mark(matchWarning, {
    acrossElements: true,
    "separateWordSearch": false,
    className: 'ir-cloze'
  });

  // Restore the selection
  editor.selection.moveToBookmark(bookmark);
}

function tinyMCEdoCloze(){
  var editor = tinyMCE.activeEditor;
  // Get content
  var selectedText = editor.selection.getContent();


  var completeText = editor.getContent();

  // Format text accordingly
  editor.selection.setContent(
    "{{c"+
    getNewClozeNumber(completeText) +
    "::" +
    selectedText +
    "}}");

  completeText = editor.getContent();

  runMark();

  // send over extract to Python
  pycmd("tir-tinymce-cloze:"+completeText);
}

function tinyMCEdoExtract(){
  var editor = tinyMCE.activeEditor;

  // unmark first
  $(editor.getBody()).unmark();

  var selectedText = editor.selection.getContent();


  // Format text accordingly
  editor.selection.setContent(
    "<div class=\"ir-extract\">" +
    selectedText +
    "</div>");

  // send over extract to Python
  pycmd("tir-tinymce-extract:"+selectedText);

  // mark again
  runMark();

}


function getNewClozeNumber(content) {
  var extractednumbers = [];
  let regex = /{{c(\d+)::([\s\S]*?)}}/g;

  var extract_clozes = content.match(regex);
  var x = 0;

  if(extract_clozes){
    for(x = 0; x < extract_clozes.length; x++){
      extractednumbers[x] = parseInt(extract_clozes[x].replace(regex, "$1"));
    }
    return Math.max(...extractednumbers)+1;
  } else {
    return 1;
  }
}



function saveHTML() {
  global_content = tinymce.activeEditor.getContent();
}
