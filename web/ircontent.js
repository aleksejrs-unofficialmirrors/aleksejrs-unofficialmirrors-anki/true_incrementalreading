var global_content    = "Placeholder content";
var global_data       = "Placeholder data";
var global_type       = "Placeholder tpye";
var global_clozecolor = "skyblue";

// P D F C O N T E N T
function loadPDF(){
  document.getElementById("htmlcontent").innerHTML = `
  <form method="post">
    <textarea id="mytextarea">Hello, World!</textarea>
  </form>
  `;
}

function savePDF(){
  global_content = "Placeholder"
}

function loadNoType(){
  document.getElementById("htmlcontent").innerHTML = "Couldnt find type!";
}

function loadContent(){
  // Init content from fields
  global_type         = document.getElementById("tir-type").innerHTML;
  global_content      = document.getElementById("tir-content").innerHTML;
  global_data         = document.getElementById("tir-data").innerHTML;

  // Init color selection
  global_clozecolor   = document.getElementById("tir-clozecolor").innerHTML;
  global_extractcolor = document.getElementById("tir-extractcolor").innerHTML;

  // Init shortcut names for tooltips (handled over qt)
  global_sc_cloze     = document.getElementById("tir-shortcut-cloze").innerHTML;
  global_sc_extract   = document.getElementById("tir-shortcut-extract").innerHTML;

  switch (global_type) {
    case "pdf"    : loadPDF(); break;
    case "youtube": loadYouTube(); break;
    case "html"   : loadHTML(); break;
    case "video"  : loadYouTube(); break;
    default       : loadNoType(); break;
  }
}

function saveContent(){
  switch (global_type) {
    case "pdf"    : savePDF(); break;
    case "youtube": saveYouTube(); break;
    case "html"   : saveHTML(); break;
    case "video"  : saveYouTube(); break;
    default       : saveNoType(); break;
  }
  pycmd("tir-receive-content:"+global_content);
  pycmd("tir-receive-data:"   +global_data);
  pycmd("tir-actually-save");
}
