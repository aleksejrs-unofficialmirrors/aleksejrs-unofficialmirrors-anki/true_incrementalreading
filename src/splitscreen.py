import aqt
from aqt import mw
from aqt.addcards import AddCards
from aqt.qt import *

from .config import get_config_value
from .rightscreen import RightScreen

class MakeSplitscreen:
    def __init__(self, editor, ir_note):
        self.web = editor.web
        addcards = self.web.parent().parent()

        if not isinstance(addcards, AddCards):
            addcards = mw.app.activeWindow()
        assert(isinstance(addcards, AddCards))

        layout = addcards.layout()
        hbox = QHBoxLayout()
        vbox = QVBoxLayout()

        while layout.count() > 0:
            item = layout.takeAt(0)
            if item.widget() is None or isinstance(item, QBoxLayout):
                vbox.addLayout(item.layout())
            else:
                vbox.addWidget(item.widget())

        temp = QWidget()
        temp.setLayout(layout)

        flds = QWidget()
        flds.setLayout(vbox)

        hbox.setSpacing(0)
        hbox.setContentsMargins(0, 0, 0, 0)

        rightscreen = RightScreen(ir_note)

        width_note = get_config_value("width_note")
        hbox.addWidget(flds, 100 - width_note)
        hbox.addWidget(rightscreen, width_note)

        addcards.setLayout(hbox)
        addcards.update()

        addcards.form.deckArea.setMinimumWidth(35)
        addcards.form.modelArea.setMinimumWidth(35)

        addcards.helpButton.setVisible(False)
