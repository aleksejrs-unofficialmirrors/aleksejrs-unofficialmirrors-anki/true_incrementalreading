from .consts import *

from aqt import mw, gui_hooks
from aqt.addcards import AddCards
from aqt.qt import *
from aqt.utils import showInfo, tooltip
from anki.notes import Note

from .config import get_config_value
from .tirmodel import TIRreturnCurrentFieldnames


class NewTrueIR_Note():
    def setDid(self, did):
        self.did = did

    def saveCard(self, did):
        model = mw.col.models.byName(get_config_value("tir-modelname"))
        model['did'] = did

        note = Note(mw.col, model)

        for (note_name, note_value) in note.items():
            for (field_name, field_value) in self._fieldnames.items():
                if field_value == note_name:
                    note[note_name] = self.field[field_name];

        mw.col.addNote(note)

    def __init__(self):
        self._fieldnames = TIRreturnCurrentFieldnames()

        keys = self._fieldnames.keys()
        self.field = {key: "" for key in keys}

class TrueIR_Note:
    def _init_fieldnames(self):
        """get current settings"""
        self._fieldnames = TIRreturnCurrentFieldnames()

        """Initialize TrueIR.fields"""
        keys = self._fieldnames.keys()
        self.field = {key: None for key in keys}

    def saveCard(self):
        note = self.note

        for (note_name, note_value) in note.items():
            for (field_name, field_value) in self._fieldnames.items():
                if field_value == note_name:
                    note[note_name] = self.field[field_name];

        note.flush()

        tooltip("Saved IR Note.")


    def __init__(self, card):
        """Check if we have a true IR card here"""
        if not card or not card.model()["name"] == get_config_value("tir-modelname"):
            self.isIR = False
            return None

        self.card = card

        """Is IR card"""
        self.isIR = True

        """initialize the fieldnames from the config"""
        self._init_fieldnames()

        """create note from card"""
        self.note = card.note()
        note      = self.note


        """Fill TrueIR_Note-fields with values from the note"""
        for (note_name, note_value) in note.items():
            for (field_name, field_value) in self._fieldnames.items():
                if field_value == note_name:
                    self.field[field_name] = note[note_name];
