from aqt.qt import *
from aqt import mw
import aqt
from anki.utils import isMac
from aqt.utils import showInfo

from aqt.webview import AnkiWebView
from .splitscreen import RightScreen


class MakeSplitscreen:
    def __init__(self, editor):
        self.web = editor.web
        addcards = self.web.parent().parent()

        if not isinstance(addcards, AddCards):
            addcards = mw.app.activeWindow()
        assert(isinstance(addcards, AddCards))

        layout = addcards.layout()
        hbox = QHBoxLayout()
        vbox = QVBoxLayout()

        while layout.count() > 0:
            item = layout.takeAt(0)
            if item.widget() is None or isinstance(item, QBoxLayout):
                vbox.addLayout(item.layout())
            else:
                vbox.addWidget(item.widget())

        temp = QWidget()
        temp.setLayout(layout)

        flds = QWidget()
        flds.setLayout(vbox)

        hbox.setSpacing(0)
        hbox.setContentsMargins(0, 0, 0, 0)

        rightscreen = RightScreen()

        hbox.addWidget(flds, 40)
        hbox.addWidget(rightscreen, 60)

        addcards.setLayout(hbox)
        addcards.update()

        addcards.form.deckArea.setMinimumWidth(35)
        addcards.form.modelArea.setMinimumWidth(35)

        addcards.helpButton.setVisible(False)
