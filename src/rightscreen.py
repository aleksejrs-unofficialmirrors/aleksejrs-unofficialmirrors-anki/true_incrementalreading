from aqt.qt import *
from aqt import mw
import aqt
from aqt.utils import showInfo
from typing import Any

from aqt.webview import AnkiWebView

from .tir_note import TrueIR_Note
from .consts import ADDON_NAME
from .config import get_config_value
from .knowledgetree import KnowledgeTree
from .notedata import NoteData

class RightScreen(QWidget):
    def initiate_save_note(self):
        """Initiate the save note, but have to wait for pycmd"""
        self._web.eval("saveContent();")

    def actually_save_note(self):
        """reset tir_note to the changed note"""
        self.tir_note = self.changed_note

        showInfo(self.tir_note.field["Data"])

        self.tir_note.saveCard()

    def save_and_rep(self):
        self.initiate_save_note()

        """do the rep"""

    def reset_note(self):
        self.tir_note = TrueIR_Note(self.tir_note.card)
        self.init_browser()

    def init_browser(self):
        addon_id     = ADDON_NAME

        type         = self.tir_note.field["Type"]
        content      = self.tir_note.field["Content"]
        data         = self.tir_note.field["Data"]

        clozecolor   = get_config_value("color-cloze")
        extractcolor = get_config_value("color-extract")

        sc_cloze     = get_config_value("hotkey-cloze")
        sc_extract   = get_config_value("hotkey-extract")

        self._web.stdHtml(f"""
        <html>
        <body id='htmlcontent'>
        <div id="tir-type">{type}</div>
        <div id="tir-content">{content}</div>
        <div id="tir-data">{data}</div>

        <div id="tir-clozecolor">{clozecolor}</div>
        <div id="tir-extractcolor">{extractcolor}</div>

        <div id="tir-shortcut-cloze">{sc_cloze}</div>
        <div id="tir-shortcut-extract">{sc_extract}</div>
        </body>
        </html>
        """,
        css = [
        f"../_addons/{addon_id}/web/style.css"
        ],
        js=[
        # third-pary libraries
        f"../_addons/{addon_id}/web/tinymce/tinymce.min.js",
        f"../_addons/{addon_id}/web/jquery/jquery-3.5.1.min.js",
        f"../_addons/{addon_id}/web/jquery/jquery.mark.min.js",

        # own stuff
        f"../_addons/{addon_id}/web/ircontent.js",
        f"../_addons/{addon_id}/web/ir_html.js",
        f"../_addons/{addon_id}/web/ir_youtube.js"
        ])

        self._web.eval("loadContent();")

    # INITIALIZE KEYBOARD SHORTCUTS
    def init_shortcuts(self):
        """Cloze deletion shortcut"""
        self.shortcut_clozedeletion = QShortcut(get_config_value("hotkey-cloze"), self)
        self.shortcut_clozedeletion.activated.connect(self.shortcutCloze)

        self.shortcut_extract       = QShortcut(get_config_value("hotkey-extract"), self)
        self.shortcut_extract.activated.connect(self.shortcutExtract)


    def toggletree(self):
        """
            Toggle bookmarks visibility.
        """
        if self.knowledgetree.isHidden():
            self.knowledgetree.setVisible(True)
        else:
            self.knowledgetree.setVisible(False)

    def _on_bridge_cmd(self, cmd: str) -> Any:
        if cmd.startswith("tir-tinymce-cloze"):
            (save_cmd, clozetext) = cmd.split(":", 1)


        elif cmd.startswith("tir-tinymce-extract"):
            (save_cmd, test) = cmd.split(":", 1)
            showInfo("Extract:" + test)

        elif cmd.startswith("tir-receive-content"):
            (save_cmd, self.changed_note.field["Content"]) = cmd.split(":", 1)

        elif cmd.startswith("tir-receive-data"):
            (save_cmd, self.changed_note.field["Data"]) = cmd.split(":", 1)

        elif cmd.startswith("tir-actually-save"):
            self.actually_save_note()

    def editmeta(self):
        notedata = NoteData(self.changed_note)

        changes = notedata.changed_note
        if changes:
            self.changed_note = changes

        self.title_label.setText(self.changed_note.field["Title"])


    def shortcutCloze(self):
        # check if cloze can be sent
        if(self.type == "html"):
            self._web.eval("tinyMCEdoCloze();")

    def shortcutExtract(self):
        # check if extract can be sent
        if(self.type == "html"):
            self._web.eval("tinyMCEdoExtract();")

    def __init__(self, tir_note, parent = None):
        QWidget.__init__(self, parent)
        self.tir_note = tir_note
        self.changed_note = tir_note

        self.type = self.tir_note.field["Type"];

        """Add Shortcuts"""
        self.init_shortcuts()


        """Make vertical boxes for the Title/Content/Scheduling section"""
        box_screensections = QVBoxLayout()

        """Make horizontal box for the Title/Edit line"""
        box_title = QHBoxLayout()

        button_meta = QPushButton("Meta")
        button_meta.clicked.connect(self.editmeta)

        button_save = QPushButton("Save")
        button_save.clicked.connect(self.initiate_save_note)

        button_hidetree = QPushButton("...")
        button_hidetree.clicked.connect(self.toggletree)
        self.title_label = QLabel(self.tir_note.field["Title"])

        box_title.addWidget(self.title_label, 85)
        box_title.addWidget(button_save, 10)
        box_title.addWidget(button_meta, 10)
        box_title.addWidget(button_hidetree, 5)

        """Create webview"""
        self._web = AnkiWebView(title="incremental_reader")
        self._web.set_bridge_command(self._on_bridge_cmd, self)

        self.init_browser();

        """Scheduling section"""
        box_schedule = QHBoxLayout()

        button_noteup = QToolButton()
        button_noteup.setArrowType(Qt.UpArrow)

        button_noteleft = QToolButton()
        button_noteleft.setArrowType(Qt.LeftArrow)

        button_noteright = QToolButton()
        button_noteright.setArrowType(Qt.RightArrow)

        button_notedown = QToolButton()
        button_notedown.setArrowType(Qt.DownArrow)

        button_resetnote = QPushButton("Reset")
        button_resetnote.clicked.connect(self.reset_note)
        button_saveschedule = QPushButton("Save and Rep")
        button_saveschedule.clicked.connect(self.save_and_rep)

        self.field_scheduledate = QLineEdit()

        box_schedule.addWidget(QLabel("Due on:"))
        box_schedule.addWidget(self.field_scheduledate)
        box_schedule.addWidget(button_resetnote)
        box_schedule.addWidget(button_noteup)
        box_schedule.addWidget(button_notedown)
        box_schedule.addWidget(button_noteleft)
        box_schedule.addWidget(button_noteright)
        box_schedule.addWidget(button_saveschedule)

        """Add Widgets to main Widget"""
        box_screensections.addLayout(box_title)
        box_screensections.addWidget(self._web)
        box_screensections.addLayout(box_schedule)

        """Create KnowledgeTree"""
        self.knowledgetree = QWidget()
        vbox_knowledgetree = QVBoxLayout()

        label = QLabel("tst")

        knowledgetree = KnowledgeTree("splitscreen")

        vbox_knowledgetree.addWidget(knowledgetree, 2)

        self.knowledgetree.setLayout(vbox_knowledgetree)

        """Add Widgets to main main layout"""
        box_main = QHBoxLayout()

        width_kt = get_config_value("width_knowledgtree")
        box_main.addLayout(box_screensections, 100-width_kt)
        box_main.addWidget(self.knowledgetree, width_kt)

        self.knowledgetree.setVisible(False)

        self.setLayout(box_main)
