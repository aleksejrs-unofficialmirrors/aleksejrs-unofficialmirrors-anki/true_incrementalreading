import base64
import requests
import random
from glob import glob
from datetime import datetime
import os
import time
from aqt import mw
from aqt.qt import *
from aqt.utils import tooltip, showInfo
from urllib.parse import urlparse


def file_exists(full_path):
    if full_path is None or len(full_path) < 2:
        return False
    return os.path.isfile(full_path)

def base64_to_file(b64):
    base_path = get_user_files_folder_path()
    # ugly
    fname = str(random.randint(10000000, 99999999))
    with open(base_path + fname + ".png", "wb") as f:
        f.write(base64.b64decode(b64))
    return base_path + fname + ".png"

"""def pdf_to_base64(path):
    with open(path, "rb") as pdf_file:
        encoded_string = base64.b64encode(pdf_file.read()).decode("ascii")
    return encoded_string"""

def get_user_files_folder_path():
    """
    Path ends with /
    """
    dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))).replace("\\", "/")
    if not dir.endswith("/"):
        return dir + "/user_files/"
    return dir + "user_files/"
