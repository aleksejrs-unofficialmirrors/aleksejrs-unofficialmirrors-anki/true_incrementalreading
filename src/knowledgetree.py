from aqt.qt import *

class KnowledgeTree(QTreeWidget):
    def __init__(self, modus):
        QTreeWidget.__init__(self)
        self.modus = modus

        if modus == "splitscreen":
            self.initSplitKT()
        elif modus == "bigscreen":
            self.initBigKT()
        else:
            return None

    def initSplitKT(self):
        self.setColumnCount(2)

        self.setHeaderLabels(["Name", "Tag"])


        l1 = QTreeWidgetItem(["Name", "Tag"])
        l1.addChild(QTreeWidgetItem(["test", "test3"]))

        self.addTopLevelItem(l1)

    def initBigKT(self):
        self.setColumnCount(3)
