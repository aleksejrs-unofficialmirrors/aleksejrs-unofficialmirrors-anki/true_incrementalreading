from aqt.qt import *
from aqt import mw

from aqt.utils import showInfo

from .config import get_config_value, update_config
from .setting_tabs.appearancetab import AppearanceTab
from .setting_tabs.hotkeytab import HotkeyTab
from .setting_tabs.behaviourtab import BehaviourTab

class SettingsDialog(QDialog):
    def __init__(self, parent):
        if not parent:
            parent = mw.app.activeWindow()

        QDialog.__init__(self, parent)

        self.setWindowTitle("TrueIR Settings")
        self.setup_ui()
        self.exec_()

    def setup_ui(self):
        self.vbox = QVBoxLayout()

        self.hotkey = get_config_value("hotkey-cloze")

        self.tabs           = QTabWidget()
        self.appearance_tab = AppearanceTab()
        self.behaviour_tab  = BehaviourTab()
        self.hotkeys_tab    = HotkeyTab()

        self.tabs.addTab(self.appearance_tab, "Appearance")
        self.tabs.addTab(self.behaviour_tab , "Behaviour")
        self.tabs.addTab(self.hotkeys_tab   , "Hotkeys")

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok|QDialogButtonBox.Cancel)
        self.buttonBox.accepted.connect(self.accept_clicked)
        self.buttonBox.rejected.connect(self.reject)

        self.vbox.addWidget(QLabel("Some settings may or may not need a restart!"))
        self.vbox.addWidget(self.tabs)
        self.vbox.addWidget(self.buttonBox)

        self.setLayout(self.vbox)

    def accept_clicked(self):
        self.appearance_tab.saveChanges()

        self.accept()
