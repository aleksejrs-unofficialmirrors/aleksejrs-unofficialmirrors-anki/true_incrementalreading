from aqt.qt import *
from aqt import mw
import aqt
from aqt.utils import showInfo

from ..tir_note import NewTrueIR_Note
from .downloadweb import *

class WebImporter(QDialog):
    def __init__(self, parent = None):
        if not parent:
            parent = mw.app.activeWindow()

        QDialog.__init__(self, parent)

        self.setWindowTitle("Web Import")
        self.setup_ui()
        self.exec_()

    def loadUrl(self):
        language = self.edit_language.text()
        article  = self.edit_article.text()

        url = f"""https://{language}.wikipedia.org/wiki/{article}?action=render"""

        self.webpage = downloadWeb(url, "web")

    def setup_ui(self):
        vbox = QVBoxLayout()

        grid = QGridLayout()

        self.edit_language = QLineEdit("en")
        self.edit_article  = QLineEdit()

        grid.addWidget(QLabel("Language:"), 0,0)
        grid.addWidget(QLabel("Article"), 1,0)

        grid.addWidget(self.edit_language, 0, 1)
        grid.addWidget(self.edit_article, 1, 1)


        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.accept_clicked)
        buttonBox.rejected.connect(self.reject)

        vbox.addLayout(grid)
        vbox.addWidget(buttonBox)

        self.setLayout(vbox)

    def accept_clicked(self):
        self.loadUrl()

        if self.webpage is None:
            return

        tir = NewTrueIR_Note()
        tir.field["Title"]   = self.edit_article.text()
        tir.field["Author"]  = "wikipedia-article"
        tir.field["Content"] = self.webpage
        tir.field["Type"]    = "html"

        self.accept()
