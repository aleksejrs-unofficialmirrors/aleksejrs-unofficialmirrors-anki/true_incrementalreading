from bs4 import BeautifulSoup
from urllib.parse import urljoin
import urllib.request
import urllib.error

from aqt.utils import showInfo

"""
    url is obiously the URL, type can be
    "wikipedia" (add ), "web" (currently not different)
"""
def downloadWeb(url, type = None):
    try:
        page = urllib.request.urlopen(url)
    except urllib.error.URLError as e:
        showInfo(f"""<b>Error:</b> {e}<br>Could not retrieve Url:<br>{url}""")
        return None

    soup = BeautifulSoup(page, "html.parser")

    for a in soup.find_all(href=True):
        a["href"] = urljoin(url, a["href"])


    if type == "wikipedia":
        # fix images
        for img in soup.find_all("img", src=True):
            img["src"] = urljoin("https://upload.wikimedia.org", img["src"])

        # remove stupid navboxes on bottom
        for div in soup.find_all("div", {'class':'navbox'}):
            div.decompose()

        for div in soup.find_all("div", {'class':'metadata'}):
            div.decompose()
            
    elif type == "web":
        for img in soup.find_all("img", src=True):
            img["src"] = urljoin(url, img["src"])

    return soup.prettify()
