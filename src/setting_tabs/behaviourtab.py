from aqt.qt import *
from aqt import mw

from aqt.utils import showInfo

from ..tirmodel import TIRcreateModel
from ..config import get_config_value, update_config

class BehaviourTab(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        grid = QVBoxLayout()

        button_updatemodel = QPushButton("Create Notetype!")
        button_updatemodel.clicked.connect(self.updateModel)

        grid.addWidget(QLabel("""
            Pressing this button will create your IR notetype.<br>
            Customising an existing one is currently not possible.<br>
            Use browser Change Note and carefully adjust the fields.
        """))
        grid.addWidget(button_updatemodel)

        self.setLayout(grid)

    def updateModel(self):
        if(TIRcreateModel()):
            showInfo("""
                Created model according to settings.<br>
                Please merge possible existing cards using the browser.""")
        else:
            showInfo("The model already exists.")
