from aqt.qt import *
from aqt import mw

from aqt.utils import showInfo

from ..config import get_config_value, update_config

class AppearanceTab(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.setupUi()
        self.setupValues()

    def setupValues(self):
        self.colorcloze   = get_config_value("color-cloze")
        self.colorextract = get_config_value("color-extract")


        self.changeButtonColor(self.colorcloze_btn,  self.colorcloze)
        self.changeButtonColor(self.colorextract_btn,self.colorextract)

    def setupUi(self):
        grid = QGridLayout()

        self.colorcloze_btn = QPushButton()
        self.colorextract_btn = QPushButton()

        self.colorcloze_btn.clicked.connect(lambda _,
            t="color-cloze",   b=self.colorcloze_btn:   self.getNewColor(t, b))
        self.colorextract_btn.clicked.connect(lambda _,
            t="color-extract", b=self.colorextract_btn: self.getNewColor(t, b))

        grid.addWidget(QLabel("<b>Cloze color:</b>"),0,0)
        grid.addWidget(self.colorcloze_btn, 0, 1)
        grid.addWidget(QLabel("<b>Extract color:</b>"), 1,0)
        grid.addWidget(self.colorextract_btn, 1, 1)

        self.setLayout(grid)

    def getNewColor(self, clrvar, clrbtn):
        """Set color via color selection dialog"""
        dialog = QColorDialog()
        color = dialog.getColor()

        if color.isValid():
            # Remove the # sign from QColor.name():
            color = color.name()[1:]
            if clrvar == "color-cloze":
                self.colorcloze = color
            elif clrvar == "color-extract":
                self.colorextract = color
            self.changeButtonColor(clrbtn, color)

    def changeButtonColor(self, button, color):
        """Generate color preview pixmap and place it on button"""
        pixmap = QPixmap(64, 18)
        qcolour = QColor(0, 0, 0)
        qcolour.setNamedColor('#' + color)
        pixmap.fill(qcolour)
        button.setIcon(QIcon(pixmap))
        button.setIconSize(QSize(128, 18))

    def saveChanges(self):
        update_config("color-cloze"  , self.colorcloze)
        update_config("color-extract", self.colorextract)
