from aqt.qt import *
from aqt import mw
import aqt
from aqt.utils import showInfo

from .tir_note import NewTrueIR_Note
from .tirmodel import TIRreturnCurrentFieldnames

"""
    handles editing metadata + scheduling
    type ... can be any of the types
    new ... whether card is new or not
    parent ... QT parent item
    tir_note ... true incremental reading note to edit
"""
class NoteData():
    def __init__(self, tir_note, new = False, parent = None):
        """check if we have a parent item"""
        if not parent:
            parent = mw.app.activeWindow()

        self.qdialog = QDialog(parent = parent)

        self.curfieldnames = TIRreturnCurrentFieldnames()
        self.new          = new
        self.tir_note     = tir_note
        self.type         = tir_note.field["Type"]
        self.changed_note = tir_note

        self.qdialog.setWindowTitle("Edit Data")
        self.setup_ui()
        self.qdialog.exec_()


    def setup_ui(self):
        vbox = QVBoxLayout()

        tabs = QTabWidget()

        ui_metadata  = self.ui_metadata()
        ui_scheduler = self.ui_scheduler()

        tabs.addTab(ui_metadata, "Metadata")
        tabs.addTab(ui_scheduler, "Scheduling")

        buttonbox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonbox.accepted.connect(self.saveEdit)
        buttonbox.rejected.connect(self.qdialog.reject)

        vbox.addWidget(tabs)
        vbox.addWidget(buttonbox)

        self.qdialog.setLayout(vbox)

    def ui_metadata(self):
        metadata_tab = QWidget()
        metadata_gbox = QGridLayout()

        keys = self.curfieldnames.keys()
        self.editfield = {key: "" for key in keys}
        self.fieldname = {key: "" for key in keys}

        i = 0
        for (internfield, userfield) in self.curfieldnames.items():
            self.fieldname[internfield] = QLabel(userfield)
            self.editfield[internfield] = QLineEdit(self.changed_note.field[internfield])

            metadata_gbox.addWidget(self.fieldname[internfield],i,0)
            metadata_gbox.addWidget(self.editfield[internfield],i,1)
            i+=1

        metadata_tab.setLayout(metadata_gbox)

        return metadata_tab

    def ui_scheduler(self):
        scheduling_tab = QWidget()

        sched_vbox = QVBoxLayout()

        sched_vbox.addWidget(QLabel("some minor test"))
        sched_vbox.addWidget(QLabel("some other minor test I guess"))

        scheduling_tab.setLayout(sched_vbox)

        return scheduling_tab

    def saveEdit(self):
        for (internfield, userfield) in self.curfieldnames.items():
            self.changed_note.field[internfield] = self.editfield[internfield].text()

        if self.new:
            self.changed_note.saveCard(mw.col.decks.id("Default"))
            self.qdialog.accept()
        else:
            # don't save, rather let coder retrieve from changed_note
            self.qdialog.accept()

    def closeEvent(self, QCloseEvent):
        self.changed_note = None
        showInfo("Nothing saved.")
        self.qdialog.reject()
