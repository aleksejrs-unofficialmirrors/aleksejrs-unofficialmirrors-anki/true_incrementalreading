from .config import get_config_value
from aqt.utils import showInfo

from aqt import mw

template_css = """
.card {
    font-family     : arial;
    font-size       : 15px;
    text-align      : left;
    color           : black;
    background-color: white;
}

#tir_info {
    background-color: #76BAF6;
    padding         : 10px;
    border-radius   : 10px;
}
"""

def TIRreturnTemplate(template, fieldnames):
    template['qfmt'] = """
        <div id="tir_info">
            This is a True Incremental Reading card.
        </div>
    """

    """fill with all the fields we got, baby!"""
    for (name, set_value) in fieldnames.items():
        template['qfmt'] += '<b>'+set_value+':</b>{{'+set_value+'}}<br>'


    template['afmt'] = """
        <div id="tir_info">
            Consider using the add-ons Add-Dialog for scheduling. </div>
    """

    return template

def TIRreturnCurrentFieldnames():
    fieldnames = {
        'Type'           : get_config_value("fname_type"          ),
        'Author'         : get_config_value("fname_author"        ),
        'Title'          : get_config_value("fname_title"         ),
        'Publication'    : get_config_value("fname_publication"   ),
        'Volume'         : get_config_value("fname_volume"        ),
        'Issue'          : get_config_value("fname_issue"         ),
        'Doi'            : get_config_value("fname_doi"           ),
        'Issn'           : get_config_value("fname_issn"          ),
        'Url'            : get_config_value("fname_url"           ),
        'Date'           : get_config_value("fname_date"          ),
        'DateAdded'      : get_config_value("fname_dateadded"     ),
        'Content'        : get_config_value("fname_content"       ),
        'ParentTag'      : get_config_value("fname_parenttag"     ),
        'Data'           : get_config_value("fname_data"          ),
        'Comment'        : get_config_value("fname_comment"       ),
        'DefaultDeck'    : get_config_value("fname_defaultdeck"   ),
        'AdditionalTags' : get_config_value("fname_additionaltags"),
        'SourceString'   : get_config_value("fname_sourcestring"  ),
        'NoteName'       : get_config_value("fname_notename"      )
    }
    return fieldnames

def TIRcreateModel():
    if checkTIRModel():
        """return false if the model already existed"""
        return False
    col = mw.col

    fieldnames    = TIRreturnCurrentFieldnames()
    tir_modelname = get_config_value("tir-modelname")
    tir_cardname  = get_config_value("tir-cardname")

    tir_model = col.models.byName(tir_modelname)



    models = col.models
    tir_model = models.new(tir_modelname)

    for (n, v) in fieldnames.items():
        fld = models.newField(fieldnames[n])
        #if n=="BLAH"
            #fld['size'] = 0 #hide 'BLAH' from being edited by user!
        models.addField(tir_model, fld)

    template = models.newTemplate(tir_cardname)
    template = TIRreturnTemplate(template, fieldnames)

    tir_model['css'] = template_css
    tir_model['sortf'] = 1

    models.addTemplate(tir_model, template)
    models.add(tir_model)
    return True

"""check if True Incremental Reading Model already exists."""
def checkTIRModel():
    col = mw.col
    tir_modelname = get_config_value("tir-modelname")
    tir_model = col.models.byName(tir_modelname)
    if tir_model:
        return True
    else:
        return False
