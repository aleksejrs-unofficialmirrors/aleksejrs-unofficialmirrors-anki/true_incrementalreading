import anki.stats
import aqt
from aqt import mw, gui_hooks
from aqt.addcards import AddCards
from anki.hooks import addHook
from aqt.qt import *
from aqt.utils import showInfo
from aqt.editor import Editor
from aqt.webview import AnkiWebView

from .settings import SettingsDialog
from .consts import *
from .tir_note import TrueIR_Note
from .splitscreen import MakeSplitscreen
from .config import get_config_value
from .importer.wikipedia import WikipediaImporter

from .lib.com.lovac42.anki.gui import toolbar


class IncrementalReader():
    def __init__(self):
        self.port = mw.mediaServer.getPort()

        mw.addonManager.setWebExports(ADDON_NAME, ".*\\.(js|css|map|png|svg|ttf)$")

        self.setupMenu()

        gui_hooks.editor_did_load_note.append(self.on_load_note)
        gui_hooks.reviewer_did_show_question.append(self.check_card_for_tir)

        self.activate_trueir_in_addnote = False
        self.last_card = None
        self.current_ir_note = None


    def setupMenu(self):
        menu_name = "&Incremental Reading"
        menu = toolbar.getMenu(mw, menu_name)

        """ add Import submenu """
        submenu = toolbar.getSubMenu(menu, "Import")
        MENU_OPTIONS=( # CONF_KEY, TITLE, CALLBACK
            ("hotkey-importwebpage", "Import Webpage", self.importWebpage),
            ("hotkey-importwikipedia", "Import Wikipedia", self.importWikipedia),
            ("hotkey-importyoutube", "Import Youtube", self.importYouTube),
            ("hotkey-importpdf"    , "Import PDF",     self.importPDF)
        )

        for k,t,cb in MENU_OPTIONS:
            hk = get_config_value(k)

            act=QAction(t,mw)
            if hk:
                act.setShortcut(QKeySequence(hk))
            act.triggered.connect(cb)
            submenu.addAction(act)

        """ add remaining menu options"""
        MENU_OPTIONS=( # CONF_KEY, TITLE, CALLBACK
            ("hotkey-knowledgetree","Knowledge Tree"   ,self.openKnowledgeTree),
            ("hotkey-settings"     ,"Addon Preferences",self.openSettings     )
        )

        for k,t,cb in MENU_OPTIONS:
            hk = get_config_value(k)

            act=QAction(t,mw)
            if hk:
                act.setShortcut(QKeySequence(hk))
            act.triggered.connect(cb)
            menu.addAction(act)

    def openKnowledgeTree(self):
        showInfo("knowledge tree")

    def openSettings(self):
        SettingsDialog(mw)

    def importWebpage(self):
        showInfo("Imported a webpage.")

    def importWikipedia(self):
        WikipediaImporter(mw)


    def importYouTube(self):
        showInfo("Youtube-Importer")

    def importPDF(self):
        showInfo("PDF-Importer")

    def on_load_note(self, editor):
        if editor.addMode and self.activate_trueir_in_addnote:
            MakeSplitscreen(editor, self.current_ir_note)
            self.activate_trueir_in_addnote = False

    def check_card_for_tir(self, card):
        """Gets called everytime a card is shown"""
        self.current_ir_note = TrueIR_Note(card)

        if not self.current_ir_note.isIR or card.id == self.last_card:
            self.last_card = card.id
            activate_trueir_in_addnote = False
            return

        """Open add cards menu with the right variable"""
        self.activate_trueir_in_addnote = True
        self.last_card = card.id
        AddCards(mw)
